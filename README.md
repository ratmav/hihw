hihw
====

# Install

```bash
$ cd path/to/your/checkout/hihw
$ virtualenv -p python2.7 .hihw_env
$ source .hihw_env/bin/activate
$ pip install -e .
```

# Test

```bash
$ cd path/to/your/checkout/hihw
$ source .hihw_env/bin/activate
$ python setup.py pytest
```

# Run

## Development

```bash
$ cd path/to/your/checkout/hihw
$ source .hihw_env/bin/activate
$ export FLASK_APP=hihw
$ export FLASK_DEBUG=true
$ flask run # App should be available on port 5000.
```

# Teardown

```bash
$ deactivate
```
