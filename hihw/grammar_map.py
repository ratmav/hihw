import yaml
import os

from collections import Counter


class GrammarMap:
    def __init__(self, start=1, stop=123):
        self.start, self.stop = 1, 123
        self.contents = self.all_numbers()
        self.length = len(self.contents)
        self.distribution = Counter(self.contents)
        self.graph = self.graphable(self.distribution)

    def all_numbers(self):
        words = []
        if self.stop > self.start and self.stop < 1000:
            for number in range(self.start, self.stop + 1):
                words.append(self.hundred(number))
            return ''.join(words)
        else:
            print 'Starting point must be less than stopping point and ' \
                    'stopping point must be less than 1000.'

    def graphable(self, distribution):
        gd = []
        for letter in distribution:
            gd.append({'Letter': letter, 'Count': distribution[letter]})
        return gd

    def grammar(self, prefix):
        path = os.path.join(os.path.dirname(__file__), 'numbers.yml')
        with open(path, 'r') as grammar:
            return yaml.safe_load(grammar.read())[prefix]

    def one(self, number):
        word = self.grammar('ones')[int(str(number)[-1])]
        if word != 'zero':
            return word
        else:
            return ''

    def ten(self, number):
        if 10 <= number < 20:
            return self.grammar('teens')[number]
        elif 20 <= number:
            place_value = int(str(number)[-2])
            word = self.grammar('tens')[place_value]
            if word != 'zero':
                return word + self.one(number - (place_value * 10))
            else:
                return ''
        else:
            return self.one(number)

    def hundred(self, number):
        if 99 < number:
            place_value = int(str(number)[-3])
            word = self.grammar('ones')[place_value] + 'hundred'
            if word != 'zero':
                return word + self.ten(number - (place_value * 100))
            else:
                return ''
        else:
            return self.ten(number)
