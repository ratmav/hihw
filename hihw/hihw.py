from flask import Flask, render_template, request
from flask_bootstrap import Bootstrap
import json
import grammar_map


app = Flask(__name__)
app.config['DEBUG'] = True
app.config['SECRET_KEY'] = 'super-secret'

grammar_map = grammar_map.GrammarMap(1, 123)


@app.route('/')
def app_root():
    return render_template('main.html', contents=grammar_map.contents,
                           length=grammar_map.length)


@app.route('/calculate')
def calcuate():
    letter = request.args.get('letter')
    if letter and len(letter) == 1 and letter.isalpha():
        p = grammar_map.distribution[letter]/float(grammar_map.length)
        return json.dumps(p)


@app.route('/graph')
def distribution():
    return json.dumps(grammar_map.graph)


# Bootstrap
Bootstrap(app)
