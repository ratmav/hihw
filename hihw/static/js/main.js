function calculate(letter, cb) {
  $.ajax({
    url: "/calculate",
    data: { "letter": letter },
    dataType: "json",
    success: function(data){
      cb(letter, data);
    },
    error: function (response) {
      alert('Problem calculating empirical probability.');
    },
  });
}

function validate() {
  var letter = document.getElementById('user-letter').value;
  if (letter && /^[a-zA-Z]+$/.test(letter) && letter.length == 1) {
    calculate(letter, notify);
  } else {
    invalid();
  }
}

function notify(letter, probability) {
  $("#invalidCalc").addClass("hidden");
  $("#validCalc").removeClass("hidden");
  var validCalcMessage = document.getElementById('validCalcMessage');
  validCalcMessage.innerHTML = 'The probability of the letter "' + letter
    + '" is: <strong>' + probability + '</strong>';
}

function invalid() {
  $("#validCalc").addClass("hidden");
  $("#invalidCalc").removeClass("hidden");
  var invalidCalcMessage = document.getElementById('invalidCalcMessage');
  invalidCalcMessage.innerHTML = "Please input a single character "
    + "alphabetic string.";
}

// Build distribution graph.
$.ajax({
    url: "/graph",
    dataType: "json",
    success: function(data){
      var svg = dimple.newSvg("#chartContainer", 800, 600);
      var chart = new dimple.chart(svg, data);
      chart.addCategoryAxis("x", "Letter");
      chart.addMeasureAxis("y", "Count");
      chart.addSeries(null, dimple.plot.bar);
      chart.draw();
    },
    error: function (response) {
      alert('Problem retrieving graph data.');
    }
});

// Display probability for the letter "e".
$(function() {
  calculate('e', function(letter, data) {
    var eProb= document.getElementById('e-prob');
    eProb.innerHTML = 'For example, the probability of the letter "'
      + letter + '" is: <strong>' + data + '</strong>';
  });
});

// Calculate probabilty on button click.
$(function(letter) {
  $('#calculate').on('click', function (e) {
      e.preventDefault();
      validate();
  });
});

// Calculate probabilty on Enter keypress.
$(function(letter) {
  $('#user-letter').keypress(function (e) {
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
      validate();
    }
  });
});
