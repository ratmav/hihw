from setuptools import setup

setup(
    name='hihw',
    packages=['hihw'],
    include_package_data=True,
    install_requires=[
        'flask',
        'flask-bootstrap',
        'pyyaml',
    ],
    setup_requires=[
        'pytest-runner',
    ],
    tests_require=[
        'pytest',
    ],
)
