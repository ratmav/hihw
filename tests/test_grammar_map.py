import pytest
from hihw.grammar_map import GrammarMap
from collections import Counter


@pytest.fixture
def grammar_map():
    return GrammarMap(1, 123)


def test_one(grammar_map):
    assert grammar_map.one(123) == 'three'


def test_ten(grammar_map):
    assert grammar_map.ten(123) == 'twentythree'


def test_hundred(grammar_map):
    assert grammar_map.hundred(123) == 'onehundredtwentythree'


def test_contents(grammar_map):
    contents = 'onetwothreefourfivesixseveneightnineteneleventwelvethirteen' \
            'fourteenfifteensixteenseventeeneighteennineteentwentytwentyone' \
            'twentytwotwentythreetwentyfourtwentyfivetwentysixtwentyseventw' \
            'entyeighttwentyninethirtythirtyonethirtytwothirtythreethirtyfo' \
            'urthirtyfivethirtysixthirtyseventhirtyeightthirtyninefortyfort' \
            'yonefortytwofortythreefortyfourfortyfivefortysixfortysevenfort' \
            'yeightfortyninefiftyfiftyonefiftytwofiftythreefiftyfourfiftyfi' \
            'vefiftysixfiftysevenfiftyeightfiftyninesixtysixtyonesixtytwosi' \
            'xtythreesixtyfoursixtyfivesixtysixsixtysevensixtyeightsixtynin' \
            'eseventyseventyoneseventytwoseventythreeseventyfourseventyfive' \
            'seventysixseventysevenseventyeightseventynineeightyeightyoneei' \
            'ghtytwoeightythreeeightyfoureightyfiveeightysixeightyseveneigh' \
            'tyeighteightynineninetyninetyoneninetytwoninetythreeninetyfour' \
            'ninetyfiveninetysixninetysevenninetyeightninetynineonehundredo' \
            'nehundredoneonehundredtwoonehundredthreeonehundredfouronehundr' \
            'edfiveonehundredsixonehundredsevenonehundredeightonehundrednin' \
            'eonehundredtenonehundredelevenonehundredtwelveonehundredthirte' \
            'enonehundredfourteenonehundredfifteenonehundredsixteenonehundr' \
            'edseventeenonehundredeighteenonehundrednineteenonehundredtwent' \
            'yonehundredtwentyoneonehundredtwentytwoonehundredtwentythree'
    assert contents == grammar_map.contents


def test_length(grammar_map):
    assert grammar_map.length == 1235


def test_distribution(grammar_map):
    distribution = Counter({'e': 233, 't': 160, 'n': 157, 'i': 100, 'y': 84,
                            'h': 69, 'r': 69, 'o': 68, 'f': 56, 'd': 48,
                            's': 44, 'u': 36, 'v': 36, 'w': 27, 'g': 22,
                            'x': 22, 'l': 4})
    assert grammar_map.distribution == distribution


def test_graph(grammar_map):
    graph = [{'Count': 233, 'Letter': 'e'}, {'Count': 48, 'Letter': 'd'},
             {'Count': 22, 'Letter': 'g'}, {'Count': 56, 'Letter': 'f'},
             {'Count': 100, 'Letter': 'i'}, {'Count': 69, 'Letter': 'h'},
             {'Count': 4, 'Letter': 'l'}, {'Count': 68, 'Letter': 'o'},
             {'Count': 157, 'Letter': 'n'}, {'Count': 44, 'Letter': 's'},
             {'Count': 69, 'Letter': 'r'}, {'Count': 36, 'Letter': 'u'},
             {'Count': 160, 'Letter': 't'}, {'Count': 27, 'Letter': 'w'},
             {'Count': 36, 'Letter': 'v'}, {'Count': 84, 'Letter': 'y'},
             {'Count': 22, 'Letter': 'x'}]
    assert graph == grammar_map.graph
