import pytest
from hihw import hihw


# NOTE: Needs a frontend test suite to verify DOM elements built on page load,
#       etc. via AJAX. It seems that some elements render in time for these
#       tests to pass, but others do not.

@pytest.fixture
def client(request):
    hihw.app.config['TESTING'] = True
    return hihw.app.test_client()


def test_main_page_jumbotron(client):
    page = client.get('/').data
    assert b'HIHW: Grammar Map' in page
    assert b'Integers between 1 and 123 have been spelled out in English ' \
           'with no spaces or conjunctions and compiled into a single ' \
           'string, e.g the number 101 is "onehundredone".' in page


def test_main_page_headings(client):
    page = client.get('/').data
    assert b'Empirical Probability' in page
    assert b'Contents of String' in page
    assert b'Total Length of String' in page
    assert b'Distribution of Letters in String' in page


def test_calculate_endpoint(client):
    data = client.get('/calculate?letter=e').data
    assert data == '0.18866396761133603'


def test_graph_endpoint(client):
    data = client.get('/graph').data
    distribution = '[{"Count": 233, "Letter": "e"}, ' \
                   '{"Count": 48, "Letter": "d"}, ' \
                   '{"Count": 22, "Letter": "g"}, ' \
                   '{"Count": 56, "Letter": "f"}, ' \
                   '{"Count": 100, "Letter": "i"}, ' \
                   '{"Count": 69, "Letter": "h"}, ' \
                   '{"Count": 4, "Letter": "l"}, ' \
                   '{"Count": 68, "Letter": "o"}, ' \
                   '{"Count": 157, "Letter": "n"}, ' \
                   '{"Count": 44, "Letter": "s"}, ' \
                   '{"Count": 69, "Letter": "r"}, ' \
                   '{"Count": 36, "Letter": "u"}, ' \
                   '{"Count": 160, "Letter": "t"}, ' \
                   '{"Count": 27, "Letter": "w"}, ' \
                   '{"Count": 36, "Letter": "v"}, ' \
                   '{"Count": 84, "Letter": "y"}, ' \
                   '{"Count": 22, "Letter": "x"}]'
    assert data == distribution
